set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

if(DEFINED ENV{CENTOS_DIR})
    set(CMAKE_SYSROOT $ENV{CENTOS_DIR})
elseif(DEFINED ENV{SYSROOT})
    set(CMAKE_SYSROOT $ENV{SYSROOT})
else()
    message(FATAL_ERROR "You must set CENTOS_DIR or SYSROOT environment variable")
endif()

if(DEFINED ENV{CROSS_DIR})
    set(tools $ENV{CROSS_DIR})
else()
    message(FATAL_ERROR "You must set CROSS_DIR environment variable")
endif()

if(NOT DEFINED ENV{TARGET})
    message(FATAL_ERROR "You must set TARGET environment variable")
endif()

set(CMAKE_C_COMPILER ${tools}/bin/$ENV{TARGET}-gcc)
set(CMAKE_CXX_COMPILER ${tools}/bin/$ENV{TARGET}-g++)
set(CMAKE_Fortran_COMPILER ${tools}/bin/$ENV{TARGET}-gfortran)

set(CMAKE_C_FLAGS "-mcpu=cortex-a9" CACHE STRING "CFLAGS")
set(CMAKE_C_LINK_FLAGS "-mcpu=cortex-a9" CACHE STRING "LDFLAGS")

set(CMAKE_CXX_FLAGS "-mcpu=cortex-a9 -std=c++17" CACHE STRING "CXXFLAGS")
set(CMAKE_CXX_LINK_FLAGS "-mcpu=cortex-a9 -Wl,--as-needed" CACHE STRING "LDFLAGS")

set(CMAKE_FIND_ROOT_PATH ${CMAKE_SYSROOT})

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE BOTH)

set(JAVA_HOME        "/usr/lib/jvm/java-1.8.0-openjdk" CACHE PATH "Java")
set(JAVA_JVM_LIBRARY "/usr/lib/jvm/java-1.8.0-openjdk/jre/lib/amd64/server/libjvm.so" CACHE PATH "Java")
set(JAVA_AWT_LIBRARY "/usr/lib/jvm/java-1.8.0-openjdk/jre/lib/amd64/libawt.so" CACHE PATH "Java AWT Library")
set(JAVA_INCLUDE_PATH "/usr/lib/jvm/java-1.8.0-openjdk/include" CACHE PATH "Java JNI include")
set(JAVA_INCLUDE_PATH2 "/usr/lib/jvm/java-1.8.0-openjdk/include/linux" CACHE PATH "Java JNI include")
set(JAVA_AWT_INCLUDE_PATH "/usr/lib/jvm/java-1.8.0-openjdk/include" CACHE PATH "Java AWT include")
