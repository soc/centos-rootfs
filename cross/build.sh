#!/bin/bash

set -e

export WRKDIR=/work/cross
mkdir -p $WRKDIR $CROSS_DIR
cd $WRKDIR

export COMMON_FLAGS="--prefix=$CROSS_DIR --target=$TARGET --with-sysroot=$SYSROOT"

# Build binutils
wget http://ftpmirror.gnu.org/binutils/binutils-2.37.tar.gz -O /tmp/binutils-2.37.tar.gz
tar xf /tmp/binutils-2.37.tar.gz
mkdir $WRKDIR/build-binutils && cd $WRKDIR/build-binutils
../binutils-2.37/configure $COMMON_FLAGS --disable-multilib $EXTRA_FLAGS
make -j`nproc` && make install
cd ..
rm -rf ./binutils-2.37/ ./build-binutils/

# Build mpc
wget ftp://gcc.gnu.org/pub/gcc/infrastructure/mpc-1.0.3.tar.gz -O /tmp/mpc-1.0.3.tar.gz
tar xf /tmp/mpc-1.0.3.tar.gz
mkdir $WRKDIR/build-mpc && cd $WRKDIR/build-mpc
../mpc-1.0.3/configure $COMMON_FLAGS
make -j`nproc` && make install
cd ..
rm -rf ./mpc-1.0.3/ ./build-mpc/

# Build gcc
if [[ "$CROSS_VERSION" =~ ^gcc11.* ]]; then
	GCC_VERSION=11.1.0
elif [[ "$CROSS_VERSION" =~ ^gcc8.* ]]; then
	GCC_VERSION=8.3.0
else
	echo "Unknown or missing CROSS_VERSION: '$CROSS_VERSION'"
	exit 1
fi
wget http://ftpmirror.gnu.org/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz -O /tmp/gcc-$GCC_VERSION.tar.gz
tar xf /tmp/gcc-$GCC_VERSION.tar.gz
mkdir $WRKDIR/build-gcc && cd $WRKDIR/build-gcc
../gcc-$GCC_VERSION/configure $COMMON_FLAGS --enable-languages=c,c++ --disable-multilib --with-gmp=/usr --with-mpc=$CROSS_DIR $EXTRA_FLAGS
make -j`nproc` && make install
cd ..
rm -rf ./gcc-$GCC_VERSION/ ./build-gcc/

# Build gdb
wget http://ftp.task.gda.pl/pub/gnu/gdb/gdb-11.1.tar.gz -O /tmp/gdb-11.1.tar.gz
tar xf /tmp/gdb-11.1.tar.gz
mkdir $WRKDIR/build-gdb && cd $WRKDIR/build-gdb
export PATH=$CROSS_DIR/bin:$PATH
../gdb-11.1/configure --host=$TARGET --target=$TARGET --with-sysroot=$SYSROOT --disable-multilib --prefix=$SYSROOT
make -j`nproc` && make install
cd ..
rm -rf ./gdb-11.1/ ./build-gdb/

rm /tmp/*.tar.gz
