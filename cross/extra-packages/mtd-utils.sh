#!/bin/bash

set -e

# Setup
mkdir -p ~/mtd-build/install
cd ~/mtd-build
export MTD_SRC_PATH=$PWD
export MTD_OUTPUT_PATH=$SYSROOT

# Build LZO
cd $MTD_SRC_PATH
wget http://www.oberhumer.com/opensource/lzo/download/lzo-2.10.tar.gz
tar -xzf lzo-2.10.tar.gz
cd lzo-2.10/
./configure --host=$TARGET --prefix=$MTD_OUTPUT_PATH --with-sysroot=$SYSROOT
make
make install

# Build mtd-utils
cd $MTD_SRC_PATH
git clone https://github.com/sigma-star/mtd-utils.git
cd mtd-utils
git checkout v2.1.2

export PREFIX=$MTD_OUTPUT_PATH
export LDFLAGS=-L$PREFIX/lib
export CPPFLAGS=-I$PREFIX/include

./autogen.sh
./configure --host=$TARGET \
    --prefix=$MTD_OUTPUT_PATH \
    --with-sysroot=$MTD_OUTPUT_PATH \
    --without-xattr \
    --without-zstd \
    --without-crypto
    CPPFLAGS=-I$MTD_OUTPUT_PATH/include
make
make install

rm -rf $MTD_SRC_PATH
