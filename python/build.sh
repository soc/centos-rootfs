#!/bin/bash

set -e

export WRKDIR=/work/cross
mkdir -p $WRKDIR $CROSS_DIR
cd $WRKDIR

export PATH=$CROSS_DIR/bin:$PATH

if [[ $ARCH == armv7hl ]]; then
    export PYTHON_ARCH=arm # Python does not recognize armv7hl
    export PYTHON_LIBDIR=$SYSROOT/usr/lib
else
    export PYTHON_ARCH=$ARCH
    export PYTHON_LIBDIR=$SYSROOT/usr/lib64
fi

# Build Python (3.8.14 is the latest compatible with Centos 7)
wget https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tgz -O /tmp/Python-$PYTHON_VERSION.tgz
tar xf /tmp/Python-$PYTHON_VERSION.tgz
mkdir $WRKDIR/build-python && cd $WRKDIR/build-python
# You must build natively first, then cross-compile
../Python-$PYTHON_VERSION/configure
make -j`nproc` && make install
# https://stackoverflow.com/questions/23185341/relocations-in-generic-elf-em-40
make clean
# https://bugs.python.org/issue3754
echo 'ac_cv_file__dev_ptmx=no' > /tmp/config.site
echo 'ac_cv_file__dev_ptc=no' >> /tmp/config.site
CONFIG_SITE=/tmp/config.site ../Python-$PYTHON_VERSION/configure --host=$TARGET --target=$TARGET --build=$PYTHON_ARCH --prefix=$SYSROOT --includedir=$SYSROOT/usr/include --libdir=$PYTHON_LIBDIR --enable-shared --disable-ipv6 --with-ensurepip
make -j`nproc` && make install
ln -sv /usr/lib64/python3.8/lib-dynload $SYSROOT/usr/lib/python3.8/lib-dynload
cp $SYSROOT/include/python3.8/pyconfig.h $SYSROOT/usr/include/python3.8/pyconfig.h
ulimit -n 1024
chroot $SYSROOT $SYSROOT/usr/bin/python3 -m ensurepip
cd ..
rm -rf ./Python-$PYTHON_VERSION/ ./build-python/

rm /tmp/Python-$PYTHON_VERSION.tgz